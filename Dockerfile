ARG NODE_IMAGE="dobrebydlo/node16:16.20.1-a138-0.0.2"

FROM ${NODE_IMAGE}

ARG VUE_VERSION="2.7.14"

ENV VUE_VERSION=${VUE_VERSION}

USER root

RUN npm install --location=global \
    npm \
    node-gyp \
    webpack \
    vue@${VUE_VERSION} \
    @vue/cli-service \
    @vue/cli-plugin-babel \
    @vue/cli-plugin-eslint \
    @vue/cli-plugin-typescript \
    @vue/cli-plugin-unit-jest \
    @vue/cli-plugin-router \
    @vue/cli-plugin-vuex

USER ${UNAME}
